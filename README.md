# GreetingLightRest
Simple REST API Spring Boot Application

## Overall:
This Application demonstrates how REST API works using Spring Data.
Unit Tests are included.

## Project contents:

* Spring Boot - Base of application that maintain application runtime
* Spring Data - Module that allows to persist (save objects) to database (HSQLDB in this scope)
*  HSQLDB Database - Database that performs serialization/deserialization during in-memory execution

## Project requirements: 

* Java 8 installed
* Maven 3.0 (3.9 recomended)
* Any IDE (optional)

## How to execute application

```
git clone https://reborn0105@bitbucket.org/reborn0105/greetinglightrest.git
cd ./greetinglightrest
mvn spring-boot:run
```
Once application started, visit http://localhost:8080/api/greetings to see results